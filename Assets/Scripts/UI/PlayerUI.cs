﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private TMP_Text curSpeedText;

    private void OnEnable()
    {
        EventManager.OnPlayerSpeedChange += UpdatePlayerSpeed;
    }

    private void UpdatePlayerSpeed(float curSpeed)
    {
        curSpeed = (int) (curSpeed * 10); //faking high speed
        curSpeedText.text = curSpeed.ToString(); //we could make it | curSpeed + "km\hr" | but i want more simplicity
    }
}
