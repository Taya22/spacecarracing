﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] private Transform gasPedal;
    [SerializeField] private GameObject speedOMeter;
    [SerializeField] private GameObject tutorialTextGO;

    private Sequence gasPedalScale;

    private void OnEnable()
    {
        EventManager.OnStartPlaying += OnStartPlaying;
        EventManager.OnAdStart += OnAdStart;
    }

    private void OnAdStart()
    {
        speedOMeter.SetActive(false);
        gasPedalScale = DOTween.Sequence()
            .Append(gasPedal.DOScale(1.15f, .3f))
            .Append(gasPedal.DOScale(1f, .3f));
        gasPedalScale.SetLoops(-1, LoopType.Restart);
    }

    private void OnStartPlaying()
    {
        gasPedalScale.Kill();
        tutorialTextGO.SetActive(false);
        speedOMeter.SetActive(true);
        gasPedal.localScale = new Vector3(1f, 1f, 1f);
    }
}
