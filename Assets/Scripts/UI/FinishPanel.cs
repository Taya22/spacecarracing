﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinishPanel : MonoBehaviour
{
    [SerializeField] private Image screenDimm;
    [SerializeField] private Transform panel;
    [SerializeField] private Transform continueButton;
    [SerializeField] private Transform[] yellowStars;
    [SerializeField] private Transform[] rewardItems;
    
    [Header("Store Debug")]
    [SerializeField] private GameObject storeButton;
    [SerializeField] private GameObject storePanel;
    
    void OnEnable()
    {
        EventManager.OnPlayerFinished += EnableFinishPanel;
        ResetUI();
    }

    private void ResetUI()
    {
        screenDimm.color = Color.clear;
        panel.localScale = new Vector3(0, 0, 0);
        continueButton.localScale = new Vector3(0, 0, 0);
        SetPartsActive(false);
        storeButton.SetActive(false);
        ResetTransformArray(yellowStars);
        ResetTransformArray(rewardItems);
    }

    private void ResetTransformArray(Transform[] array)
    {
        foreach (var item in array)
        {
            item.localScale = new Vector3(0, 0, 0);
        }
    }

    private void EnableFinishPanel()
    {
        StartCoroutine(EnablePanelRoutine());
    }

    IEnumerator EnablePanelRoutine()
    {
        yield return new WaitForSeconds(1f);
        
        SetPartsActive(true);
        screenDimm.DOColor(Color.white, .5f);
        panel.DOScale(1f, .6f);
        
        yield return new WaitForSeconds(.6f);
        yield return StartCoroutine(ShowTransformArray(yellowStars, Ease.OutElastic));
        yield return StartCoroutine(ShowTransformArray(rewardItems, Ease.Linear, 1.45f, 0.15f, 0.1f));
        continueButton.DOScale(1f, .3f);
        storeButton.SetActive(true);
    }

    IEnumerator ShowTransformArray(Transform[] array, 
        Ease easeType = Ease.Linear,
        float endScale = 1f, 
        float duration = 0.3f, 
        float pauseBetweenTweens = 0.2f)
    {
        var i = 0;
        while (i < array.Length)
        {
            array[i].DOScale(endScale, duration).SetEase(easeType);
            yield return new WaitForSeconds(duration + pauseBetweenTweens);
            i++;
        }
    }

    private void SetPartsActive(bool isActive)
    {
        panel.gameObject.SetActive(isActive);
        screenDimm.gameObject.SetActive(isActive);
    }

    public void GoToStore()
    {
        Debug.Log("Go to store button clicked.");
        storePanel.SetActive(true);
    }
}
