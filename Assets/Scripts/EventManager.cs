﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    #region Singleton

    

    public static EventManager Instance;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);
    }
    #endregion

    private void Start()
    {
        TriggerOnAdStart();
    }

    public delegate void StartPlaying();
    public static event StartPlaying OnStartPlaying;
    public void TriggerOnStartPlaying()
    {
        OnStartPlaying?.Invoke();
    }

    public delegate void AdStart();
    public static event AdStart OnAdStart;
    private void TriggerOnAdStart()
    {
        OnAdStart?.Invoke();
    }
    
    public delegate void PlayerSpeedChange(float curSpeed);
    public static event PlayerSpeedChange OnPlayerSpeedChange;
    public void TriggerSpeedChange(float curSpeed)
    {
        OnPlayerSpeedChange?.Invoke(curSpeed);
    }

    public delegate void PlayerFinished();
    public static event PlayerFinished OnPlayerFinished;
    public void TriggerOnPlayerFinished()
    {
        OnPlayerFinished?.Invoke();
    }
    
    public delegate void PlayExplosionOnPoint(Vector3 pos);
    public static event PlayExplosionOnPoint OnPlayExplosion;
    public void TriggerPlayExplosion(Vector3 pos)
    {
        OnPlayExplosion?.Invoke(pos);
    }
}
