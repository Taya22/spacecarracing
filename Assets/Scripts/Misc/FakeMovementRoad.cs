﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeMovementRoad : MonoBehaviour
{
    [SerializeField] private float offsetSpeed;

    private bool fakeMovementEnabled;
    
    Renderer roadRenderer;

    private void OnEnable()
    {
        EventManager.OnAdStart += EnableFakeMovement;
        EventManager.OnPlayerFinished += DisableFakeMovement;
    }

    void Start()
    {
        roadRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if(!fakeMovementEnabled) return;
        var curOffset = roadRenderer.material.mainTextureOffset;
        roadRenderer.material.mainTextureOffset = new Vector2(curOffset.x + offsetSpeed * Time.deltaTime, curOffset.y);
    }

    private void EnableFakeMovement()
    {
        fakeMovementEnabled = true;
    }

    private void DisableFakeMovement()
    {
        fakeMovementEnabled = false;
    }
}
