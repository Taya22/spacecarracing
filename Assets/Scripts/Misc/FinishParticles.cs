﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishParticles : MonoBehaviour
{
    void OnEnable()
    {
        EventManager.OnPlayerFinished += PlayFinishParticles;
    }

    private void PlayFinishParticles()
    {
        GetComponent<ParticleSystem>().Play();
    }
}
