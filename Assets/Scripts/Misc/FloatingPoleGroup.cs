﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingPoleGroup : MonoBehaviour
{
    [SerializeField]private float speed;
    [SerializeField] private bool directionForward;
    private Vector3 dir;

    private void OnEnable()
    {
        EventManager.OnPlayerSpeedChange += ChangeSpeed;
    }

    void Start()
    {
        dir = directionForward ? transform.forward : -transform.forward;
    }

    void FixedUpdate()
    { 
        Move();   
    }

    private void ChangeSpeed(float playerSpeed)
    {
        speed = playerSpeed * 4;
    }
    
    private void Move()
    {
        transform.localPosition += dir * (speed * Time.deltaTime);
        CheckForMaxDistance();
    }

    private void CheckForMaxDistance()
    {
        if (transform.position.z <= 0)
        {
            var pos = transform.position;
            transform.position = new Vector3(pos.x, pos.y,412f);
        }
    }
}
