﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//landscape only
public class SpeedParticlesController : MonoBehaviour
{
    private ParticleSystem ps;
    private float maxSpeed = 16f;
    private void OnEnable()
    {
        ps = GetComponent<ParticleSystem>();

        EventManager.OnPlayerSpeedChange += ChangeParticlesSpeed;
    }

    private void ChangeParticlesSpeed(float playerSpeed)
    {
        var mainModule = ps.main;
        
        mainModule.simulationSpeed = Mathf.Lerp(0f, 1f, playerSpeed / maxSpeed);

        if(mainModule.simulationSpeed < .3f) ps.Stop(); 
        else if(!ps.isEmitting) ps.Play();
    }
}
