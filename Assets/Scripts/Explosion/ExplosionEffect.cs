﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class ExplosionEffect : MonoBehaviour
{
    //ideally we should use object pool, but it's unnecessary because we'll have only 2-3 explosions in this demo
    private void Start()
    {
        //can be also destroyed after particleSyst.main.duration, but it's also unnecessary in our case
        var impulse = GetComponent<CinemachineImpulseSource>();
        impulse.GenerateImpulse(Camera.main.transform.forward);
        Destroy(gameObject, 2f);
    }
}
