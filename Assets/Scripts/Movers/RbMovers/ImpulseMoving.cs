﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpulseMoving : RacingVehicle
{
    [Header("Child Class")]
    [SerializeField] private float impulseCooldown;
    [SerializeField] private Vector3 dir;
    [SerializeField] private float force;
    
    void Start()
    {
        StartCoroutine(CheckForMoveEnabled());
    }

    private IEnumerator CheckForMoveEnabled()
    {
        while (true)
        {
            if (canMove)
            {
                Move();
                rb.velocity = Vector3.zero;
                yield break;
            }

            //cause it's not a player it's okay to have a little delay, so we don't rly need to check it each frame
            yield return new WaitForSeconds(0.2f);
        }
    }
    
    IEnumerator CreateImpulse()
    {
        while (true)
        {
            if(!canMove) yield break;
            
            rb.AddForce(dir.normalized * (force), ForceMode.Impulse);
            yield return new WaitForSeconds(impulseCooldown);
        }
    }

    protected override void Move()
    {
        StartCoroutine(CreateImpulse());
    }
}
