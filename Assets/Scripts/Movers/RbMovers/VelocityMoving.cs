﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityMoving : RacingVehicle
{
    [Header("Child Class")]
    [SerializeField] private float speed;

    void FixedUpdate()
    {
        Move();
    }

    //Time.deltaTime used in FixedUpdate returns Time.fixedDeltaTime. 
    //Source: https://docs.unity3d.com/ScriptReference/Time-deltaTime.html
    //"When this is called from inside MonoBehaviour.FixedUpdate, it returns Time.fixedDeltaTime."
    protected override void Move()
    {
        if(!canMove) return;
        
        rb.velocity = transform.forward * speed;
    }
}
