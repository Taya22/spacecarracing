﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceMoving : RacingVehicle
{
    [Header("Child Class")]
    [SerializeField] private Vector3 dir;
    [SerializeField] private float acceleration;
    
    void FixedUpdate()
    {
        Move();
    }

    protected override void Move()
    {
        if(!canMove) return;
        
        rb.AddForce(transform.forward * acceleration);
    }
}
