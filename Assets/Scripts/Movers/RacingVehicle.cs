﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody))]
public abstract class RacingVehicle : MonoBehaviour
{
    [Header("Parent class")]
    [SerializeField] private bool useRbToMove;

    protected Rigidbody rb;
    protected bool canMove;
    
    private GameObject explosion;
    private Sequence fakeMovement;

    //Might was better to use virtual method with if(!canMove) check, but whatever
    protected abstract void Move();

    private void OnEnable()
    {
        EventManager.OnAdStart += StartFakeMovement;
        EventManager.OnStartPlaying += StopFakeMovement;
    }

    private void Awake()
    {
        if(useRbToMove) rb = GetComponent<Rigidbody>();
        explosion = Resources.Load<GameObject>("Explosion");
    }
    
    private void StartFakeMovement()
    {
        canMove = false;
        StartCoroutine(StartFakeMovementRoutine());
    }

    private IEnumerator StartFakeMovementRoutine()
    {
        var startDelay = Random.Range(0f, 1f);
        yield return new WaitForSeconds(startDelay);

        //to prevent softlock when players start the game before startDelay passed
        if(canMove) yield break;
        
        var posChange = Random.Range(1f, 3f);
        var duration = Random.Range(4f, 6f);
        var easeType = Ease.Linear;

        fakeMovement = DOTween.Sequence()
            .Append(transform.DOMoveZ(transform.position.z + posChange, duration).SetEase(easeType))
            .Append(transform.DOMoveZ(transform.position.z, duration + 1f).SetEase(easeType));
        fakeMovement.SetLoops(-1, LoopType.Restart);
    }

    private void StopFakeMovement()
    {
        fakeMovement.Kill();
        canMove = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("backCollider"))
        {
            OnHit(other.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("FinishTrigger"))
        {
            canMove = false;
            if (gameObject.CompareTag("Player"))
            {
                EventManager.Instance.TriggerOnPlayerFinished();
            }
        }
    }

    private void OnHit(Transform bumpedObj)
    {
        Instantiate(explosion, bumpedObj.position, Quaternion.identity);
        EventManager.Instance.TriggerPlayExplosion(bumpedObj.position);
        Destroy(bumpedObj.parent.gameObject);
    }
}
