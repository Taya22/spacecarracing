﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

public class DoTweenMoving : RacingVehicle
{
    [Header("Child Class")]
    [SerializeField] private Vector3 endPos;
    [SerializeField] private float duration;

    private void Start()
    {
        StartCoroutine(CheckForMoveEnabled());
    }

    private IEnumerator CheckForMoveEnabled()
    {
        while (true)
        {
            if (canMove)
            {
                Move();
                yield break;
            }

            //cause it's not a player it's okay to have a little delay, so we don't rly need to check it each frame
            yield return new WaitForSeconds(0.2f);
        }
    }

    protected override void Move()
    {
        transform.DOMove(endPos, duration).SetEase(Ease.Linear);
    }
}
