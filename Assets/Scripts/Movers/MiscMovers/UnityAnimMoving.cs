﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityAnimMoving : RacingVehicle
{
    //Have some minor issues with FakeMovement, which can be hidden from the player, so they don't really worth fixing

    [Header("Child Class")]
    [SerializeField] [Range(0, 1)] private float animationSpeed;
    
    private Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
        _animator.enabled = false;
        StartCoroutine(CheckForMoveEnabled());
    }
    
    private IEnumerator CheckForMoveEnabled()
    {
        while (true)
        {
            if (canMove)
            {
                Move();
                yield break;
            }

            //cause it's not a player it's okay to have a little delay, so we don't rly need to check it each frame
            yield return new WaitForSeconds(0.2f);
        }
    }
    
    protected override void Move()
    {
        _animator.enabled = true;
    }

}
