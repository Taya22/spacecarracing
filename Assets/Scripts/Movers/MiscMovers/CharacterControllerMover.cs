﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerMover : RacingVehicle
{
    //Character controller isn't really a good way to implement movement in such game..
    [Header("Child class")] 
    [SerializeField] private float speed;
    
    private CharacterController _controller;
    
    void Start()
    {
        _controller = GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
        if(!canMove) return;
        Move();
    }

    protected override void Move()
    {
        _controller.Move(transform.forward * (speed * Time.deltaTime));
    }

    //CC trash methods
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.transform.GetChild(0).CompareTag("backCollider"))
        {
            OnControllerHit(hit.transform);
        }
    }
    
    private void OnControllerHit(Transform bumpedObj)
    {
        Instantiate(Resources.Load<GameObject>("Explosion"), bumpedObj.position, Quaternion.identity);
        EventManager.Instance.TriggerPlayExplosion(bumpedObj.position);
        Destroy(bumpedObj.gameObject);
    }
}
