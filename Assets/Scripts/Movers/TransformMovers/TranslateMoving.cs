﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateMoving : RacingVehicle
{
    [Header("Child Class")]
    [SerializeField] private Vector3 dir;
    [SerializeField] private float speed;

    void FixedUpdate()
    {
        Move();
    }

    protected override void Move()
    {
        if(!canMove) return;
        
        transform.Translate(dir.normalized * (speed * Time.deltaTime));
    }
}
