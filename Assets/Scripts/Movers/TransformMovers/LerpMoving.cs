﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpMoving : RacingVehicle
{
    [Header("Child Class")]
    [SerializeField] private Vector3 endPos;
    [Tooltip("% per sec")]
    [SerializeField] private float step;
    private Vector3 startPos;
    private float progress;
    
    void Start()
    {
        startPos = transform.position;
    }

    void FixedUpdate()
    {
        if(!canMove) return;
        Move();
    }

    protected override void Move()
    {
        transform.position = Vector3.Lerp(startPos, endPos, progress);
        progress += step * Time.deltaTime;
    }
}
