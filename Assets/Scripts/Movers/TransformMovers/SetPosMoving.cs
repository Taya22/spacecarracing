﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPosMoving : RacingVehicle
{
    [Header("Child Class")]
    [SerializeField] private float speed;

    private void FixedUpdate()
    {
        Move();
    }

    protected override void Move()
    {
        if(!canMove) return;
        
        transform.position += transform.forward * (speed * Time.deltaTime);
    }
}
