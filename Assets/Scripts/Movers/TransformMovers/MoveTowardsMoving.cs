﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsMoving : RacingVehicle
{
    [Header("Child Class")]
    //usually you serialize GameObject and use target.transform.position 
    [SerializeField] private Vector3 targetPos;
    [SerializeField] private float step;
    
    private float progress;

    void FixedUpdate()
    {
       Move();
    }

    protected override void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPos, progress);
        progress = step * Time.deltaTime;
    }
}
