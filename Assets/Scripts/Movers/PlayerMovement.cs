﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : RacingVehicle
{
    [Header("Child Class")]
    [SerializeField] private float speedPerClick = 0.5f;
    [SerializeField] float maxSpeed = 13f;

    //also a startSpeed
    private float curSpeed = 8f;
    
    private float speedDecreaseMultiplier = 1.5f;
    private bool startedPlaying;
    private Coroutine decreaseSpeedRoutine;

    private void Start()
    {
        OnSpeedChange();
    }

    void FixedUpdate()
    {
        if(!startedPlaying) return;
        Move();
    }

    public void IncreaseSpeed()
    {
        if (!startedPlaying)
        {
            EventManager.Instance.TriggerOnStartPlaying();
            startedPlaying = true;
        }
        if(!canMove) return;

        curSpeed += speedPerClick;
        if (curSpeed > maxSpeed) curSpeed = maxSpeed;
        
        if (decreaseSpeedRoutine == null) decreaseSpeedRoutine = StartCoroutine(DecreaseSpeedRoutine());
    }

    IEnumerator DecreaseSpeedRoutine()
    {
        while (curSpeed > 0)
        {
            speedDecreaseMultiplier = canMove ? 1.2f : 10f;
            curSpeed -= (speedDecreaseMultiplier * Time.deltaTime);
            yield return null;
        }

        curSpeed = 0;
        decreaseSpeedRoutine = null;
    }

    protected override void Move()
    {
        if (curSpeed < 0) return;

        OnSpeedChange();
        rb.velocity = transform.forward * curSpeed;
    }

    private void OnSpeedChange()
    {
        EventManager.Instance.TriggerSpeedChange(curSpeed);
    }
}
