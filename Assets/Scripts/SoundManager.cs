﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private void OnEnable()
    {
        EventManager.OnPlayExplosion += PlayExplosionOnPoint;
        EventManager.OnPlayerFinished += PlayFinishSound;
    }

    private void PlayFinishSound()
    {
        var cameraAudioSource = Camera.main.GetComponent<AudioSource>();
        cameraAudioSource.PlayOneShot(Resources.Load("Sounds/Finish") as AudioClip);
    }

    private void PlayExplosionOnPoint(Vector3 pos)
    {
        AudioSource.PlayClipAtPoint(Resources.Load("Sounds/Explosion") as AudioClip, pos);
    }
}
